/* -*- mode: C; mode: folding; fill-column: 70; -*- */
#define _XOPEN_SOURCE 600
#define _LARGEFILE64_SOURCE 1
#define _FILE_OFFSET_BITS 64

#include "stinger-atomics.h"
#include "stinger-utils.h"
#include "stinger.h"
#include "timer.h"
#include "xmalloc.h"
#include "bfs.h"
#include<string.h>
#define INC 1


static int64_t nv, ne, naction;
static int64_t *restrict off;
static int64_t *restrict from;
static int64_t *restrict ind;
static int64_t *restrict weight;
static int64_t *restrict action;

/* handles for I/O memory */
static int64_t *restrict graphmem;
static int64_t *restrict actionmem;

static char *initial_graph_name = INITIAL_GRAPH_NAME_DEFAULT;
static char *action_stream_name = ACTION_STREAM_NAME_DEFAULT;

static long source=128;
static long dest=381;

static long batch_size = BATCH_SIZE_DEFAULT;
static long nbatch = NBATCH_DEFAULT;

static struct stinger *S;

//extern int frontarr[nv];

void
parse_args1 (const int argc, char *argv[],
            char **initial_graph_name, char **action_stream_name, int64_t* source, int64_t* dest,
            int64_t * batch_size, int64_t * nbatch)
{
	  int k = 1;
  int seen_batch = 0, seen_nbatch = 0;
  if (k >= argc)
    return;
  while (k < argc && argv[k][0] == '-') {
    if (0 == strcmp (argv[k], "--batch") || 0 == strcmp (argv[k], "-b")) {
      if (seen_batch)
        goto err;
      seen_batch = 1;
      ++k;
      if (k >= argc)
        goto err;
      *batch_size = strtol (argv[k], NULL, 10);
      if (batch_size <= 0)
        goto err;
      ++k;
    } else if (0 == strcmp (argv[k], "--num-batches")
             || 0 == strcmp (argv[k], "-n")) {
      if (seen_nbatch)
        goto err;
      seen_nbatch = 1;
      ++k;
      if (k >= argc)
        goto err;
      *nbatch = strtol (argv[k], NULL, 10);
      if (nbatch < 0)
	goto err;
      ++k;
    } else if (0 == strcmp (argv[k], "--help")
             || 0 == strcmp (argv[k], "-h") || 0 == strcmp (argv[k], "-?")) {
      usage (stdout, argv[0]);
      exit (EXIT_SUCCESS);
      return;
    } else if (0 == strcmp (argv[k], "--")) {
      ++k;
      break;
    }
  }
  if (k < argc)
    *initial_graph_name = argv[k++];
  if (k < argc)
    *action_stream_name = argv[k++];
if (k < argc)
   *source = strtol(argv[k++],NULL,10);
if (k < argc)
    *dest = strtol(argv[k++],NULL,10);

  return;
 err:
  usage (stderr, argv[0]);
  exit (EXIT_FAILURE);
  return;
}





int
main (const int argc, char *argv[])
{
 //int c;
  parse_args1 (argc, argv, &initial_graph_name, &action_stream_name,&source,&dest,
              &batch_size, &nbatch);
  STATS_INIT ();

  load_graph_and_action_stream (initial_graph_name, &nv, &ne,
                                (int64_t **) & off, (int64_t **) & ind,
                                (int64_t **) & weight,
                                (int64_t **) & graphmem, action_stream_name,
                                &naction, (int64_t **) & action,
                                (int64_t **) & actionmem);

  print_initial_graph_stats (nv, ne, batch_size, nbatch, naction);
  BATCH_SIZE_CHECK ();
  /* Convert to STINGER */
  tic ();
/*---Adding File read and Stingerification---Shwetha---
  FILE *fopen;
  int64_t count=0;
  int64_t offset[nv];
  fopen=open("inputgraph_directed.txt","r");
  char s[20];
  while(fgets(s,sizeof(s),fopen))
  {
	if(indexOf('[')==(indexOf(']')-1))
	count++;
	
	
  }




----Edit Ends----*/
  S = stinger_new ();

  stinger_set_initial_edges (S, nv, 0, off, ind, weight, NULL, NULL, 0);
   PRINT_STAT_DOUBLE ("time_stinger", toc ());
  fflush (stdout);
//   PRINT_STAT_DOUBLE("off",off);
   int64_t numSteps = 3;
  int64_t src_dest_pair[2] = { source, dest };

  tic ();
  int64_t size_intersect = st_conn_stinger (S, nv, ne, src_dest_pair, 1, numSteps);
  PRINT_STAT_DOUBLE ("time_st_conn_stinger", toc ());
  PRINT_STAT_INT64 ("size_intersect", size_intersect);

  stinger_free_all (S);
  free (graphmem);
  free (actionmem);
  STATS_END ();
}


void
bfs_stinger (const struct stinger *G, const int64_t nv, const int64_t ne,
             const int64_t startV,
             int64_t * marks, const int64_t numSteps,
             int64_t * Q, int64_t * QHead, int64_t * neighbors, int *frontarr)
{
  int64_t j, k, s;
  int64_t nQ, Qnext, Qstart, Qend;
  int64_t w_start,v;
	int c,i;
	for(c=0;c<nv;++c)
        frontarr[c]=-1;
	
	for(c=0;c<nv;++c)
	marks[c]=0;
	size_t d,deg_v;
	int64_t my_w;	
	Q[0]=startV;
	nQ=0,j=0;
	marks[startV]=1;
	int markcount=1;
	v=startV;
	frontarr[v]=0;
	//v=Q[j];
	while(nQ<nv && markcount<=nv && j<nv)
	{
		
		//v=Q[j];	
		deg_v=stinger_outdegree(G,v);
		//printf("degree of %d is %d\t",v,deg_v);
		my_w=nQ+deg_v;
		stinger_gather_typed_successors (G, 0, v, &d, neighbors, deg_v);
		
		for(i=0;i<deg_v;i++)
		{
		//printf("neighbors = %d \t",neighbors[i]);
		if(marks[neighbors[i]]!=1)
		{
		frontarr[neighbors[i]]=frontarr[v]+1;
		marks[neighbors[i]]=1;
		markcount++;
		Q[nQ++]=neighbors[i];	
		}
		}
		v=Q[j++];
		//printf("nQ=%d markcount=%d\n",nQ,markcount);		
		
	}
//	printf("\nfrontarr\n");
//	for(c=0;c<nv;++c)
//	printf("%d \t",frontarr[c]);
//	printf("\n");
	

}

//	int frontarr[nv];
/*for(c=0;c<nv;++c)
	frontarr[c]=-1;
int frontier=0;
  MTA ("mta assert nodep")
    for (j = 0; j < nv; j++) {
      marks[j] = 0;

    }

  s = startV;*/
  /* Push node s onto Q and set bounds for first Q sublist */
 /* Q[0] = s;
  Qnext = 1;
  nQ = 1;
  QHead[0] = 0;
  QHead[1] = 1;
  marks[s] = 1;
  frontarr[0]=0;
*/
 // frontarr[s] = 0;
/* PushOnStack:    */               /* Push nodes onto Q */

  /* Execute the nested loop for each node v on the Q AND
     for each neighbor w of v  */
/*  Qstart = QHead[nQ - 1];
  Qend = QHead[nQ];
  w_start = 0;
  
    MTA ("mta assert no dependence")
    MTA ("mta block dynamic schedule")
    for (j = Qstart; j < Qend; j++) {
      int64_t v = Q[j];
	
      size_t d;
      size_t deg_v = stinger_outdegree (G, v);
	int64_t my_w = w_start+deg_v;
	w_start+=deg_v;

        stinger_gather_typed_successors (G, 0, v, &d, &neighbors[my_w], deg_v);
	

      assert (d == deg_v);

	 for(c=(w_start-deg_v);c<deg_v;c++)
                frontarr[neighbors[c]]=frontarr[v]+1;
	
	//w_start
	

//	frontarr[j]=frontier;
      MTA ("mta assert nodep")
        for (k = 0; k < deg_v; k++) {
          int64_t d, w, l;
		
	  
          w = neighbors[my_w + k];*/
	// printf("\n w=%d my_w=%d k=%d",(int)w,(int)my_w,k); 
          /* If node has not been visited, set distance and push on Q */
    /*      if ( (marks[w]+1) == 0) {
		marks[w]+=1;
		Q[Qnext+1] = w;
		Qnext++;
		frontarr[w]+=1;	
          }
        }
	
    }
*/	
   	//printf("nq=%d\n",nQ);
  /*if (Qnext != QHead[nQ]){//&& nQ < numSteps) {
    nQ++;
    QHead[nQ] = Qnext;
    goto PushOnStack;
  }
		
	printf("\n Q\n");
		 for(c=0;c<nv;++c)
        printf("%d\t",Q[c]);
	
	printf("\n marks\n");
	for(c=0;c<nv;c++)
	printf("%d\t",marks[c]);	
  	printf("\n");

	printf("\n frontier\n");
        for(c=0;c<nv;c++)
        printf("%d\t",frontarr[c]);
        printf("\n");


	
}
*/

int64_t
st_conn_stinger (const struct stinger *G, const int64_t nv, const int64_t ne,
                 const int64_t * sources, const int64_t num,
                 const int64_t numSteps)
{
  int64_t k, x;
  int frontarr[nv];
  int64_t *Q_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *marks_s_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *marks_t_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *QHead_big =
    (int64_t *) xmalloc (INC * 2 * numSteps * sizeof (int64_t));
  int64_t *neighbors_big = (int64_t *) xmalloc (INC * ne * sizeof (int64_t));
int c;
  int64_t count = 0;
  FILE *res;
  res=fopen("results.txt","w");
  k = 0;
  x = 0;

  OMP("omp parallel for")
    MTA ("mta assert parallel")
    MTA ("mta loop future")
    MTA ("mta assert nodep")
    MTA ("mta assert no alias")
    for (x = 0; x < INC; x++) {
      int64_t *Q = Q_big + x * nv;
      int64_t *marks_s = marks_s_big + x * nv;
      int64_t *marks_t = marks_t_big + x * nv;
      int64_t *QHead = QHead_big + x * 2 * numSteps;
      int64_t *neighbors = neighbors_big + x * ne;
	int i;
      for (int64_t claimedk = stinger_int64_fetch_add (&k, 2);
           claimedk < 2 * num; claimedk = stinger_int64_fetch_add (&k, 2)) {
        int64_t s = sources[claimedk];
        int64_t deg_s = stinger_outdegree (G, s);
        int64_t t = sources[claimedk + 1];
        int64_t deg_t = stinger_outdegree (G, t);

        if (deg_s == 0 || deg_t == 0) {
          stinger_int64_fetch_add (&count, 1);
        } else {
          bfs_stinger (G, nv, ne, s, marks_s, numSteps, Q, QHead, neighbors,frontarr);
          //bfs_stinger (G, nv, ne, t, marks_t, numSteps, Q, QHead, neighbors);
          int64_t local_count = 0;

          MTA ("mta assert nodep")
            for (int64_t j = 0; j < nv; j++) {
              if (marks_s[j] && marks_t[j])
                stinger_int64_fetch_add (&local_count, 1);
            }

          if (local_count == 0)
            stinger_int64_fetch_add (&count, 1);
        }
	// fprintf(res,"%d %d %d \n",(int)s,(int)t,abs(marks_s[s]-marks_t[t]));

	fprintf(res,"%d %d %d \n",(int)s,(int)t,(frontarr[t]));
//	for(c=0;c<nv;++c)
//	printf("%d\t",Q[c]);

 
      }
	
//	for(i=0;i<nv;++i)
  //      printf("Marks marks marks!\n %d %d \n %d %d \n",(int)s,(int)marks_s[i],(int)t,(int)marks_t[i]);

   }
  free (neighbors_big);
  free (QHead_big);
  free (marks_t_big);
  free (marks_s_big);
  free (Q_big);
  fclose(res);
  return count;

}


int64_t
st_conn_stinger_source (const struct stinger * G, const int64_t nv,
                        const int64_t ne, const int64_t from,
                        const int64_t * sources, const int64_t num,
                        const int64_t numSteps)
{
  int64_t k;

  int64_t *Q = (int64_t *) xmalloc (nv * sizeof (int64_t));
  int64_t *marks_s = (int64_t *) xmalloc (nv * sizeof (int64_t));
  int64_t *marks_t = (int64_t *) xmalloc (nv * sizeof (int64_t));
  int64_t *QHead = (int64_t *) xmalloc (2 * numSteps * sizeof (int64_t));
  int64_t *neighbors = (int64_t *) xmalloc (ne * sizeof (int64_t));

  int64_t count = 0;
int frontarr[nv];
  int64_t deg_s = stinger_outdegree (G, from);
  if (deg_s == 0) {
    return num;
  }

  bfs_stinger (G, nv, ne, from, marks_s, numSteps, Q, QHead, neighbors,frontarr);

  for (k = 0; k < num; k++) {
    int64_t t = sources[k];
    int64_t deg_t = stinger_outdegree (G, t);

    if (deg_t == 0) {
      stinger_int64_fetch_add (&count, 1);
    } else {
      bfs_stinger (G, nv, ne, t, marks_t, numSteps, Q, QHead, neighbors,frontarr);

      int64_t local_count = 0;

      MTA ("mta assert nodep")
        for (int64_t j = 0; j < nv; j++) {
          if (marks_s[j] && marks_t[j])
            stinger_int64_fetch_add (&local_count, 1);
        }

      if (local_count == 0)
        stinger_int64_fetch_add (&count, 1);
    }
  }

  free (neighbors);
  free (QHead);
  free (marks_t);
  free (marks_s);
  free (Q);

  return count;

}


void
bfs_csr (const int64_t nv, const int64_t ne, const int64_t * off,
         const int64_t * ind, const int64_t startV, int64_t * marks,
         const int64_t numSteps, int64_t * Q, int64_t * QHead)
{
  int64_t j, k, s;
  int64_t nQ, Qnext, Qstart, Qend;


  MTA ("mta assert nodep")
    for (j = 0; j < nv; j++) {
      marks[j] = 0;
    }

  s = startV;
  /* Push node s onto Q and set bounds for first Q sublist */
  Q[0] = s;
  Qnext = 1;
  nQ = 1;
  QHead[0] = 0;
  QHead[1] = 1;
  marks[s] = 1;

 PushOnStack:                   /* Push nodes onto Q */

  /* Execute the nested loop for each node v on the Q AND
     for each neighbor w of v  */
  Qstart = QHead[nQ - 1];
  Qend = QHead[nQ];

  MTA ("mta assert no dependence")
    MTA ("mta block dynamic schedule")
    for (j = Qstart; j < Qend; j++) {
      int64_t v = Q[j];
      int64_t myStart = off[v];
      int64_t myEnd = off[v + 1];

      MTA ("mta assert nodep")
        for (k = myStart; k < myEnd; k++) {
          int64_t d, w, l;
          w = ind[k];
          /* If node has not been visited, set distance and push on Q */
          if (stinger_int64_fetch_add (marks + w, 1) == 0) {
            Q[stinger_int64_fetch_add (&Qnext, 1)] = w;
          }
        }
    }

  if (Qnext != QHead[nQ] && nQ < numSteps) {
    nQ++;

QHead[nQ] = Qnext;
    goto PushOnStack;
  }
}
int64_t
st_conn_csr (const int64_t nv, const int64_t ne, const int64_t * off,
             const int64_t * ind, const int64_t * sources, const int64_t num,
             const int64_t numSteps)
{
  int64_t k, x;

  int64_t *Q_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *marks_s_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *marks_t_big = (int64_t *) xmalloc (INC * nv * sizeof (int64_t));
  int64_t *QHead_big =
    (int64_t *) xmalloc (INC * 2 * numSteps * sizeof (int64_t));
  int64_t *neighbors_big = (int64_t *) xmalloc (INC * ne * sizeof (int64_t));

  int64_t count = 0;

  k = 0;

  MTA ("mta assert parallel")
    MTA ("mta loop future")
    for (x = 0; x < INC; x++) {
      int64_t *Q = Q_big + x * nv;
      int64_t *marks_s = marks_s_big + x * nv;
      int64_t *marks_t = marks_t_big + x * nv;
      int64_t *QHead = QHead_big + x * 2 * numSteps;
      int64_t *neighbors = neighbors_big + x * ne;

      for (int64_t claimedk = stinger_int64_fetch_add (&k, 2);
           claimedk < 2 * num; claimedk = stinger_int64_fetch_add (&k, 2)) {

        int64_t s = sources[claimedk];
        int64_t t = sources[claimedk + 1];
        bfs_csr (nv, ne, off, ind, s, marks_s, numSteps, Q, QHead);
        bfs_csr (nv, ne, off, ind, t, marks_t, numSteps, Q, QHead);

        int64_t local_count = 0;

        MTA ("mta assert nodep")
          for (int64_t j = 0; j < nv; j++) {
            if (marks_s[j] && marks_t[j])
              stinger_int64_fetch_add (&local_count, 1);
          }

        if (local_count == 0)
          stinger_int64_fetch_add (&count, 1);
      }
    }

  free (neighbors_big);
  free (QHead_big);
  free (marks_t_big);
  free (marks_s_big);
  free (Q_big);

  return count;

}
