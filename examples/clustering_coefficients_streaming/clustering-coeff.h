#if !defined(CLUSTERING_COEFF_H_)
void count_all_triangles (const size_t, const int64_t * restrict,
			  const int64_t * restrict, int64_t * restrict);
#endif
