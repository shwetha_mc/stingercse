#+TITLE:     Streaming clustering coefficients experimental rig
#+AUTHOR:    Jason Riedy
#+EMAIL:     jason.riedy@cc.gatech.edu
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:nil skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 

* Setup

On the Cray XMT, both =gen-streams= and =main= use the snapshot I/O
interface.  You need to run fsworker from the data file directory
and set =SWORKER\_EP= to the output id string.

* Generating data

The primary parameters are =--scale=, =--edgefact=, and =--nact=.
To generate a graph of scale 24, edge factor 11 (so 2^24 vertices
and 11 * 2^24 edges), and 60 actions:
: ./gen-streams --scale=24 --edgefact=11 --nact=60 graph.bin actions.bin

Other parameters are given by running =./gen-streams --help=.  Note
that the data generator is only minimally parallelized.

* Running the experimental rig

: ./main graph.bin actions.bin > out.json

For more details on the STINGER API, see ROADMAP
